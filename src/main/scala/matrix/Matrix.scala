package matrix

import scala.reflect.ClassTag

object Matrix {

  trait Element[T] {
    def +(a: T): T = a
    def -(a: T): T
    def +(a: T, b: T): T
    def -(a: T, b: T): T
    def *(a: T, b: T): T
    def /(a: T, b: T): T
  }

  implicit object ElementDouble extends Element[Double] {
    def -(a: Double) = -a
    def +(x: Double, y: Double) = x + y
    def -(x: Double, y: Double) = x - y
    def *(x: Double, y: Double) = x * y
    def /(x: Double, y: Double) = x / y
  }

}

trait Matrix[T] {
  def dimensions: (Int, Int)

  def =~=(that: Matrix[T]): Boolean

  def apply(i: Int, j: Int): T
  def apply(ij: (Int, Int)): T = apply(ij._1, ij._2)

  def *:(that: T) = :*(that)
  def :*(that: T): Matrix[T]
  def :/(that: T): Matrix[T]

  def unary_+ = this
  def unary_- : Matrix[T]
  def +(that: Matrix[T]): Matrix[T]
  def -(that: Matrix[T]): Matrix[T]
  def *(that: Matrix[T]): Matrix[T]
}

trait SquareMatrix[T] extends Matrix[T] {
  def dimension: Int
  def det: T
}

object det {
  def apply[T](m: SquareMatrix[T]) = m.det
}

class ArrayMatrix[T: ClassTag](private val array: Array[Array[T]])(implicit mElem: Matrix.Element[T]) extends Matrix[T] {

  def =~=(that: Matrix[T]): Boolean = {
    if (this.dimensions != that.dimensions) return false
    for (
      i <- 0 until dimensions._1;
      j <- 0 until dimensions._2
    ) if (this(i, j) != that(i, j)) return false
    true
  }

  def dimensions: (Int, Int) = (array.length, array(0).length)

  def apply(i: Int, j: Int): T = array(i)(j)

  def map[B: ClassTag](f: (Int, Int, T) => B)(implicit bElem: Matrix.Element[B]) = {
    val a = Array.tabulate[B](dimensions._1, dimensions._2)((i, j) => f(i, j, this(i, j)))
    new ArrayMatrix[B](a)
  }

  def map[B: ClassTag](f: T => B)(implicit bElem: Matrix.Element[B]) = {
    val a = Array.tabulate[B](dimensions._1, dimensions._2)((i, j) => f(this(i, j)))
    new ArrayMatrix[B](a)
  }

  def map[B: ClassTag](f: (Int, Int) => B)(implicit bElem: Matrix.Element[B]) = {
    val a = Array.tabulate[B](dimensions._1, dimensions._2)((i, j) => f(i, j))
    new ArrayMatrix[B](a)
  }

  def :*(that: T): Matrix[T] = map { mElem.*(_, that) }
  def :/(that: T): Matrix[T] = map { mElem./(_, that) }

  def unary_- : Matrix[T] = map { mElem.-(_) }

  def +(that: Matrix[T]): Matrix[T] = {
    val a = Array.ofDim(dimensions._1, dimensions._2)
    for (
      i <- 0 until dimensions._1;
      j <- 0 until dimensions._2
    ) a(i)(j) = mElem.+(this(i, j), that(i, j))
    new ArrayMatrix(a)
  }
  def -(that: Matrix[T]): Matrix[T] = {
    val a = Array.ofDim(dimensions._1, dimensions._2)
    for (
      i <- 0 until dimensions._1;
      j <- 0 until dimensions._2
    ) a(i)(j) = mElem.-(this(i, j), that(i, j))
    new ArrayMatrix(a)
  }

  def *(that: Matrix[T]): Matrix[T] = {
    val a = Array.ofDim(this.dimensions._1, that.dimensions._2)
    for (
      i <- 0 until this.dimensions._2;
      j <- 0 until that.dimensions._1
    ) {
      var s = mElem.*(this(i, 0), that(0, j))
      for (n <- 1 until this.dimensions._1)
        s = mElem.+(s, mElem.*(this(i, n), that(n, j)))
      a(i)(j) = s
    }
    new ArrayMatrix(a)
  }
}

class ArraySquareMatrix[T: ClassTag](private val array: Array[Array[T]])(implicit mElem: Matrix.Element[T]) extends ArrayMatrix[T](array) with SquareMatrix[T] {
  def dimension = dimensions._1
  def det = this(0, 0)
}
