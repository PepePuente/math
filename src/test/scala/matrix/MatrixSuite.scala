package matrix

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import complex.Complex

@RunWith(classOf[JUnitRunner])
class MatrixSuite extends FunSuite {

  import matrix._
  import matrix.Matrix.ElementDouble

  test("scalar product") {
    val a = new ArrayMatrix[Double](
      Array(
        Array(1, 3, 1),
        Array(1, 0, 0)))
    val a3 = new ArrayMatrix[Double](
      Array(
        Array(3, 9, 3),
        Array(3, 0, 0)))
    assert(3.0 *: a =~= a3)
  }

  test("complex scalar product") {
    val a = new ArrayMatrix[Complex](
      Array(
        Array(1, 3, 1),
        Array(1, 0, 0)))
    val a3 = new ArrayMatrix[Complex](
      Array(
        Array(3, 9, 3),
        Array(3, 0, 0)))
    assert(3 *: a =~= a3)
  }

  test("addition") {
    val a = new ArrayMatrix[Double](
      Array(
        Array(1, 3, 1),
        Array(1, 0, 0)))
    val b = new ArrayMatrix[Double](
      Array(
        Array(0, 0, 5),
        Array(7, 5, 0)))
    val ab = new ArrayMatrix[Double](
      Array(
        Array(1, 3, 6),
        Array(8, 5, 0)))
    assert(a + b =~= ab)
    //    assert(a + b =:= ab)
  }

  test("product") {
    val a = new ArrayMatrix[Double](
      Array(
        Array(1, 2),
        Array(3, 4)))
    val b = new ArrayMatrix[Double](
      Array(
        Array(0, 1),
        Array(0, 0)))
    val ab = new ArrayMatrix[Double](
      Array(
        Array(0, 1),
        Array(0, 3)))
    val ba = new ArrayMatrix[Double](
      Array(
        Array(3, 4),
        Array(0, 0)))
    assert(a * b =~= ab)
    assert(b * a =~= ba)
    //    assert(a * b =:= ab)
    //    assert(b * a =:= ba)
  }
}
