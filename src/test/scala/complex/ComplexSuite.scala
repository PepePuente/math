package complex

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ComplexSuite extends FunSuite {

  import complex._
  import complex.Complex._

  test("angle") {
    assert(Angle(0) === Angle(2 * Math.PI))
    assert(-Angle(0) === Angle(Math.PI))
    assert(Angle(-2 * Math.PI) === Angle(2 * Math.PI))
    assert(-Angle(Math.PI) === Angle(0))
  }

  test("angle values") {
    val a = Angle(0)
    assert(0 == a(0))
    assert(2 * Math.PI == a(1))
    assert(4 * Math.PI == a(2))
    assert(-2 * Math.PI == a(-1))
    assert(-4 * Math.PI == a(-2))
  }
  
  test("i") {
    assert(i === Complex(0, 1))
    assert(i * i === Complex(-1, 0))
    assert(i * i === -1)
  }

  test("j") {
    assert(j === cis(Math.PI / 2))
    assert(-j === -cis(Math.PI / 2))
    assert(j * j === cis(Math.PI))
  }

//  test("i j") {
//    assert(i === j.toCartesian)
//  }
  
  test("j i") {
    assert(j === i.toPolar)
  }
  
  test("addition") {
    assert(3 + 2 * i === Complex(3, 2))
    assert((2 + 4 * i) + (5 - i) === (7 + 3 * i))
  }

  test("cis") {
    assert(3 * Angle(1) === Angle(3))
    assert(cis(.5) === Complex(Angle(.5)))
    assert(10 * cis(.5) === Complex(10, Angle(.5)))
  }
}
