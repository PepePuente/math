package matrix

trait LuDecomposition {
  def decompose(a: Array[Array[Double]]): Array[Array[Double]]

  protected def copy(m: Array[Array[Double]]) = {
    val n = m.length
    Array.tabulate(n, n)((i, j) => m(i)(j))
  }

  def forwardSubst(l: Array[Array[Double]], b: Array[Double]) = {
    val n = l.length
    val x = Array.ofDim[Double](n)
    val bb = b.clone()
    for (j <- 0 until n) {
      x(j) = bb(j) / l(j)(j)
      for (i <- j + 1 until n)
        bb(i) = bb(i) - l(i)(j) * x(j)
    }
    x
  }

  def backSubst(u: Array[Array[Double]], b: Array[Double]) = {
    val n = u.length
    val x = Array.ofDim[Double](n)
    val bb = b.clone()
    for (j <- n - 1 to 0) {
      x(j) = bb(j) / u(j)(j)
      for (i <- 0 until j)
        bb(i) = bb(i) - u(i)(j) * x(j)
    }
    x
  }

}

trait GaussianElimination extends LuDecomposition {
  //L has ones on its diagonal
  def decompose(m: Array[Array[Double]]) = {
    val n = m.length
    val w = Array.ofDim[Double](n)
    val a = copy(m)
    for (k <- 0 until n - 1) {
      for (j <- k + 1 until n)
        w(j) = a(k)(j)
      for (i <- k + 1 until n) {
        val alpha = a(i)(k) / a(k)(k)
        a(i)(k) = alpha
        for (j <- k + 1 until n)
          a(i)(j) = a(i)(j) - alpha * w(j)
      }
    }
    a
  }
}

trait DoolittleFactorisation extends LuDecomposition {
  //L has ones on its diagonal
  def decompose(a: Array[Array[Double]]) = {
    val n = a.length
    val lu = Array.ofDim[Double](n, n)

    def l(i: Int)(j: Int) = {
      var l_ij = a(i)(j)
      for (k <- 0 until j)
        l_ij = l_ij - lu(i)(k) * lu(k)(j)
      l_ij / lu(j)(j)
    }

    def u(i: Int)(j: Int) = {
      var u_ij = a(i)(j)
      for (k <- 0 until i)
        u_ij = u_ij - lu(i)(k) * lu(k)(j)
      u_ij
    }

    for (
      i <- 0 until n;
      j <- 0 until n
    ) lu(i)(j) = if (j < i) l(i)(j) else u(i)(j)
    lu
  }
}

trait CroutFactorisation extends LuDecomposition {
  //U has ones on its diagonal 
  def decompose(a: Array[Array[Double]]) = {
    val n = a.length
    val lu = Array.ofDim[Double](n, n)

    for (k <- 0 until n)
      lu(k)(k) = 1

    def l(i: Int)(j: Int) = {
      var l_ij = a(i)(j)
      for (k <- 0 until j) {
        l_ij = l_ij - lu(i)(k) * lu(k)(j)
      }
      l_ij
    }

    def u(i: Int)(j: Int) = {
      var u_ij = a(i)(j)
      for (k <- 0 until i)
        u_ij = u_ij - lu(i)(k) * lu(k)(j)
      u_ij / lu(i)(i)
    }

    for (j <- 0 until n) {
      for (i <- j until n)
        lu(i)(j) = l(i)(j)
      for (i <- j + 1 until n)
        lu(j)(i) = u(j)(i)
    }
    lu
  }
}
