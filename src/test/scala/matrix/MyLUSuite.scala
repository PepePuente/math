package matrix

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import complex.Complex

@RunWith(classOf[JUnitRunner])
class MyLUSuite extends FunSuite {

  import matrix._

  test("gaussian") {
    object gaussian extends GaussianElimination

    val a: Array[Array[Double]] = Array(
      Array(2, 1, 1, 3, 2),
      Array(1, 2, 2, 1, 1),
      Array(1, 2, 9, 1, 5),
      Array(3, 1, 1, 7, 1),
      Array(2, 1, 5, 1, 8))

    val decmp = gaussian.decompose(a)

    val n = a.length
    for (i <- 0 until n) {
      for (j <- 0 until n)
        print(decmp(i)(j) + ", ")
      println
    }

  }

  test("crout") {
    object crout extends CroutFactorisation

    val a: Array[Array[Double]] = Array(
      Array(2, 1, 1, 3, 2),
      Array(1, 2, 2, 1, 1),
      Array(1, 2, 9, 1, 5),
      Array(3, 1, 1, 7, 1),
      Array(2, 1, 5, 1, 8))

    val lu: Array[Array[Double]] = Array(
      Array(2, .5, .5, 1.5, 1),
      Array(1, 1.5, 1, -1.0 / 3.0, 0),
      Array(1, 1.5, 7, 0, 4.0 / 7.0),
      Array(3, -.5, 0, 7.0 / 3.0, -6.0 / 7.0),
      Array(2, 0, 4, -2, 2))

    val mylu = crout.decompose(a)
    for (i <- 0 until 3)
      for (j <- 0 until 3)
        assertAlmostEqual(mylu(i)(j), lu(i)(j))
  }

  test("doolittle") {
    object doolittle extends DoolittleFactorisation

    val a: Array[Array[Double]] = Array(
      Array(25, 5, 1),
      Array(64, 8, 1),
      Array(144, 12, 1))

    val lu: Array[Array[Double]] = Array(
      Array(25, 5, 1),
      Array(2.56, -4.8, -1.56),
      Array(5.76, 3.5, 0.7))

    val mylu = doolittle.decompose(a)

    for (i <- 0 until 3)
      for (j <- 0 until 3)
        assertAlmostEqual(mylu(i)(j), lu(i)(j))
  }

  def assertAlmostEqual(a: Double, b: Double) = assert(Math.abs(a - b) < 1e-14)
}
