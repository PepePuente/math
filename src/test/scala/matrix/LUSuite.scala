package matrix

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import complex.Complex

@RunWith(classOf[JUnitRunner])
class LUSuite extends FunSuite {

  import matrix._

  test("scalar product") {
    val dcmp = new LUdcmp[Double]
    val m = new ArraySquareMatrix[Double](Array(
      Array(2, 3),
      Array(3, -1)))
    val b = Array[Double](1, 0)
    val sol = Array[Double](1.0 / 11.0, 3.0 / 11.0)
    val s = dcmp.solve(m, b)
    assertAlmostEqual(s(0), sol(0))
    assertAlmostEqual(s(1), sol(1))
  }

  def assertAlmostEqual(a: Double, b: Double) = assert(Math.abs(a - b) < 1e-16)
}
