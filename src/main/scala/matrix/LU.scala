package matrix

class LUdcmp[T] {

  def dcmp(m: SquareMatrix[Double]): (Array[Array[Double]], Array[Int]) = {
    val scaling: Array[Double] = computeScaling(m)

    val n = m.dimension

    val lu: Array[Array[Double]] = Array.ofDim(n, n)
    for (i <- 0 until n)
      for (j <- 0 until n)
        lu(i)(j) = m(i, j)

    val indx: Array[Int] = Array.ofDim(n)

    var d = 1
    for (k <- 0 until n) {

      var imax: Int = -1
      var big = .0
      for (i <- k until n) {
        var temp = scaling(i) * Math.abs(lu(i)(k));
        if (temp > big) {
          big = temp
          imax = i;
        }
      }

      if (k != imax) {
        for (j <- 0 until n) {
          val temp = lu(imax)(j)
          lu(imax)(j) = lu(k)(j)
          lu(k)(j) = temp;
        }
        d = -d
        scaling(imax) = scaling(k)
      }
      indx(k) = imax

      if (lu(k)(k) == 0.0) lu(k)(k) = Double.MinPositiveValue
      for (i <- k + 1 until n) {
        lu(i)(k) /= lu(k)(k)
        for (j <- k + 1 until n)
          lu(i)(j) -= lu(i)(k) * lu(k)(j)
      }

    }

    (lu, indx)
  }

  private def computeScaling(m: SquareMatrix[Double]) = {
    val scaling: Array[Double] = Array.ofDim(m.dimension)
    for (i <- 0 until m.dimension) {
      var big = .0
      for (j <- 0 until m.dimension) {
        big = Math.max(big, Math.abs(m(i, j)))
      }
      assert(big != 0.0, throw new Exception("Singular matrix in LUdcmp"))
      scaling(i) = 1.0 / big
    }
    scaling
  }

  def solve(m: SquareMatrix[Double], b: Array[Double]) = {
    require(b.length == m.dimension, "LUdcmp::solve bad sizes")

    val x: Array[Double] = Array.ofDim(m.dimension)
    val (lu, indx) = dcmp(m)

    for (i <- 0 until m.dimension) x(i) = b(i)

    var sum = .0
    var ii = 0
    for (i <- 0 until m.dimension) {
      var ip = indx(i);
      sum = x(ip)
      x(ip) = x(i)
      if (ii != 0)
        for (j <- ii - 1 until i) sum -= lu(i)(j) * x(j)
      else if (sum != 0.0)
        ii = i + 1
      x(i) = sum
    }

    for (i <- m.dimension - 1 to 0 by -1) {
      sum = x(i)
      for (j <- i + 1 until m.dimension) sum -= lu(i)(j) * x(j)
      x(i) = sum / lu(i)(i)
    }

    x
  }

}
