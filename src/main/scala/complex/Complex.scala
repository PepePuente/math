package complex

import matrix.Matrix

object Complex {
  val i = Complex(0, 1)
  val j = Complex(1, Angle(Math.PI / 2))

  def apply(re: Double, im: Double): Complex = new Cartesian(re, im)
  def apply(re: Double): Complex = new Cartesian(re, 0)
  def apply(mod: Double, arg: Angle): Complex = new Polar(mod, arg)
  def apply(arg: Angle): Complex = new Polar(1, arg)

  implicit def Double2Complex(d: Double): Complex = Complex(d, 0)

  def cis(a: Angle) = Complex(a)
  def cis(d: Double) = Complex(Angle(d))

  implicit object ElementComplex extends Matrix.Element[Complex] {
    def -(a: Complex) = -a
    def +(x: Complex, y: Complex) = x + y
    def -(x: Complex, y: Complex) = x - y
    def *(x: Complex, y: Complex) = x * y
    def /(x: Complex, y: Complex) = x / y
  }
}

object Angle {
  def apply(a: Double) = new Angle(a)
  def apply(y: Double, x: Double) = new Angle(Math.atan2(y, x))

  implicit class Double4Angle(d: Double) {
    def *(a: Angle) = a * d
  }
}

trait Complex {
  def real: Double
  def imaginary: Double
  def module: Double
  def argument: Angle

  override def hashCode() =
    real.hashCode() + module.hashCode()

  def toCartesian: Complex
  def toPolar: Complex

  def unary_+(): Complex = this
  def unary_-(): Complex
  def +(c: Complex): Complex = Complex(real + c.real, imaginary + c.imaginary)
  def -(c: Complex): Complex = Complex(real - c.real, imaginary - c.imaginary)
  def *(c: Complex): Complex
  def /(c: Complex): Complex

  def ln = Math.log(module) + argument.value * Complex.j
  def ln(n: Int) = Math.log(module) + argument(n) * Complex.j
}

class Cartesian(val real: Double, val imaginary: Double) extends Complex {
  lazy val module: Double = Math.hypot(real, imaginary)
  lazy val argument: Angle = Angle(imaginary, real)

  override def toString = real + (if (imaginary >= 0) " + " + imaginary else " - " + -imaginary) + "i"

  def toCartesian: Complex = this
  def toPolar: Complex = Complex(module, argument)

  override def equals(o: Any) = o match {
    case c: Complex => real == c.real && imaginary == c.imaginary
    case n: Number  => real == n && imaginary == 0
    case _          => false
  }

  def unary_-(): Complex = Complex(-real, -imaginary)
  def *(c: Complex): Complex = {
    val re = real * c.real - imaginary * c.imaginary
    val im = imaginary * c.real + real * c.imaginary
    Complex(re, im)
  }
  def /(c: Complex): Complex = {
    val reNum = real * c.real + imaginary * c.imaginary
    val imNum = imaginary * c.real - real * c.imaginary
    val den = c.real * c.real + c.imaginary + c.imaginary
    Complex(reNum / den, imNum / den)
  }
}

class Polar(val module: Double, val argument: Angle) extends Complex {
  require(module >= 0, "Negative module for a Complex number")

  lazy val real: Double = module * argument.cos
  lazy val imaginary: Double = module * argument.sin

  override def toString = module + " cis " + argument

  def toCartesian: Complex = Complex(real, imaginary)
  def toPolar: Complex = this

  override def equals(o: Any) = o match {
    case c: Complex => module == c.module && argument == c.argument
    case n: Number  => real == n && imaginary == 0
    case _          => false
  }

  def unary_-(): Complex = Complex(module, -argument)
  def *(c: Complex): Complex = {
    val mm = module * c.module
    val aa = argument + c.argument
    Complex(mm, aa)
  }
  def /(c: Complex): Complex = {
    val mm = module / c.module
    val aa = argument - c.argument
    Complex(mm, aa)
  }
}

class Angle(v: Double) {
  val value = if (-Math.PI < v && v <= Math.PI) v else v - 2 * Math.PI * Math.floor((v + Math.PI) / (2 * Math.PI))

  def apply(n: Int): Double = v + 2 * n * Math.PI

  override def toString = (value / Math.PI) + "π"

  def unary_+() = this
  def unary_-() = if (value > 0) Angle(v - Math.PI) else Angle(value + Math.PI)
  def +(a: Angle): Angle = Angle(value + a.value)
  def -(a: Angle): Angle = Angle(value - a.value)
  def cos: Double = Math.cos(value)
  def sin: Double = Math.sin(value)

  def *(d: Double) = Angle(value * d)
  def /(d: Double) = Angle(value / d)

  override def equals(o: Any) = o match {
    case a: Angle => value == a.value
    case _        => false
  }
  override def hashCode() = value.hashCode()
}
